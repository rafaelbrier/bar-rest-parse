var _ = require('lodash');
const numberUtils = require('./number-utils');
const xlsxj = require("xlsx-to-json");
const CONSTANTS_CLASS = require('../cloud/constants-class');
const CONSTANTS_IMPORT = require('./constants-import');
const Parse = CONSTANTS_IMPORT.PARSE;

const ProcedimentoClassName = CONSTANTS_CLASS.PROCEDIMENTO.className;
const ProcedimentoClass = new Parse.Object.extend(ProcedimentoClassName);


( function () {

    let criarObjeto = function (codigo, nome, valor) {
        let objeto = new ProcedimentoClass();
        objeto.set('codigo', codigo);
        objeto.set('nome', nome);
        objeto.set('valor', valor);
        return objeto;
    }

    let importa = function () {

        xlsxj({
            input: "./tabela-procedimentos.xlsx",
            output: null,
            sheet: 'Plan2'
        }, async function (err, result) {
            if (err) {
                console.error(err);
            } else {
                let anotherResult = result
                    .filter((r) => r.NomeEspecifico && r.Valor)
                    .map((r) => {
                        try {
                            return criarObjeto(r.Codigo, r.NomeEspecifico, parseFloat(numberUtils.formatNumberToUsd(r.Valor)))
                        } catch (e) {
                            console.log(e)
                            return null;
                        }
                    });
                for (let p of anotherResult) {
                    await p.save();
                }
            }
        });

    }

    importa();

})()


