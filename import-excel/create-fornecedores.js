var _ = require('lodash');
const CONSTANTS_CLASS = require('../cloud/constants-class');
const CONSTANTS_IMPORT = require('./constants-import');
const Parse = CONSTANTS_IMPORT.PARSE;

const FornecedorClassName = CONSTANTS_CLASS.FORNECEDOR.className;
const FornecedorClass = new Parse.Object.extend(FornecedorClassName);

let criarObjeto = function(nome, cidade, uf, celular, cep) {
    let fornecedor = new FornecedorClass();
    fornecedor.set('nome', nome);
    fornecedor.set('cidade', cidade);
    fornecedor.set('uf', uf);
    fornecedor.set('celular', celular);
    fornecedor.set('cep', cep);
    return fornecedor;
}

let saveObjeto = async function(nome, cidade, uf, celular, cep) {
    let forn = criarObjeto(nome, cidade, uf, celular, cep);
    forn = await forn.save();
    return forn;
}

let salvarFornecedores = async function() {
    await saveObjeto("SIN");
    await saveObjeto("PROSS");
    await saveObjeto("FGM");
    await saveObjeto("CONEXÃO");
    await saveObjeto("ODONTOSHOPPING");
    await saveObjeto("ODONTOCIRÚRGICA");
    await saveObjeto("Map Dent's");
    await saveObjeto("Dental Sorriso");
    await saveObjeto("Lab. Sudré");
    await saveObjeto("Marco Aurélio");
    await saveObjeto("JC");
    await saveObjeto("João Carlos Reis");
    await saveObjeto("Roberta");
    await saveObjeto("Indent");
    await saveObjeto("Neodent");
}

salvarFornecedores()
    .then(res => {
        console.log(res)
    }, error => {
        console.log(error)
    })
