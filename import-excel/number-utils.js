const _ = require('lodash');

module.exports.formatNumberToUsd = function (numero) {
    if (isNaN(numero)) {
        numero = numero.replace('R$ ', '');
        var parteInteira = numero.split(',')[0].replace(/\./g, '');
        var parteDecimal = numero.split(',')[1];
        parteDecimal = (_.isUndefined(parteDecimal)) ? '' : '.' + parteDecimal.replace(/\D/g, '');

        return parseFloat(parteInteira.replace(/\D/g, '') + parteDecimal);
    }
    return parseFloat(numero);
}