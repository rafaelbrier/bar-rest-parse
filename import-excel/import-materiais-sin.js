var _ = require('lodash');
const moment = require('moment');
const numberUtils = require('./number-utils');
const xlsxj = require("xlsx-to-json");
const fs = require('fs');
const CONSTANTS_CLASS = require('../cloud/constants-class');
const CONSTANTS_GERAL = require('../cloud/constants-geral');
const CONSTANTS_IMPORT = require('./constants-import');
const Parse = CONSTANTS_IMPORT.PARSE;

const FornecedorClassName = CONSTANTS_CLASS.FORNECEDOR.className;
const FornecedorClass = new Parse.Object.extend(FornecedorClassName);

const MaterialClassName = CONSTANTS_CLASS.MATERIAL.className;
const MaterialClass = new Parse.Object.extend(MaterialClassName);


(async function() {
    

    let criarObjeto = function(codigo, descricao, preco, fornecedor) {
        let objeto = new MaterialClass();
        objeto.set('codigo', codigo);
        objeto.set('descricao', descricao);
        objeto.set('preco', preco);
        objeto.set('fornecedor', fornecedor);
        return objeto;
    }

    let importa = function(tabela, fornName, inputFile) {
        let queryFornecedor = new Parse.Query(FornecedorClassName);
        queryFornecedor.matches('nome', fornName , 'i');
        
        queryFornecedor.first()
            .then(forn => {

                xlsxj({
                    input: inputFile, 
                    output: null,
                    sheet: tabela
                  }, function(err, result) {
                    if(err) {
                      console.error(err);
                    }else {
                        
                      let anotherResult = result
                        .filter((r) => r['DISCRIMINAÇÃO'] && r['VALOR - R$'])
                        .map((r) => {
                            if(_.isEmpty(r['CÓDIGO'])) {
                                r.codigo = null;
                            }
                            try {
                                return criarObjeto(r['CÓDIGO'], r['DISCRIMINAÇÃO'], parseFloat(numberUtils.formatNumberToUsd(r['VALOR - R$'])), forn)
                            } catch(e) {
                                console.log(e)
                                return null;
                            }
                        });
                        console.log(anotherResult[0].toJSON());
                    //   let i = 0;
                    //   let j = 0;
                    //   let promises = [];
                    //   for(let p of anotherResult) {
                    //     //   await p.save();
                    //       if(!promises[j]) {
                    //         promises[j] = [];
                    //       }
                    //       promises[j].push(p.save());
                    //       i++;
                    //       if(i%100 == 0) {
                    //         // console.log("Importados:" + i);
                    //         j++;
                    //     }
                                
                    //   }
                    //   let contadorImport = 0;
                    //   for(let prom of promises){
                    //       Promise.all(prom)
                    //         .then(res => {
                    //             contadorImport += res.length
                    //             console.log('importou ', contadorImport);
                    //         }, error => {
                    //             console.log('Tabela: ', tabela)
                    //             console.log(error)
                    //         })
                    //   }

                    }
                  });
            }, error2 => {
                console.log(error2);
            })

    }

    let arrayMateriais = [
        // {
        //     nomeForn: 'sin', 
        //     nomePlanilha: 'Plan1',
        //     inputFile: "./import-excel/Implantes-SIN.xlsx"
        // },
        {
            nomeForn: 'pross', 
            nomePlanilha: 'Planilha1',
            inputFile: "./import-excel/Implantes-Pross-Dabi.xlsx"
        },
        {
            nomeForn: 'pross', 
            nomePlanilha: 'Planilha1',
            inputFile: "./import-excel/Implantes-Pross-Dabi2.xlsx"
        },
        // {
        //     nomeForn: 'fgm', 
        //     nomePlanilha: 'Tabela FGM'
        // },
        // {
        //     nomeForn: 'conexão', 
        //     nomePlanilha: 'Tabela Conexão'
        // },
        // {
        //     nomeForn: 'odontoshopping', 
        //     nomePlanilha: 'Tabela Odontoshoping'
        // },
        // {
        //     nomeForn: 'odontocirúrgica', 
        //     nomePlanilha: 'Tabela Odontocirúrgica'
        // },
        // {
        //     nomeForn: 'map dent\'s', 
        //     nomePlanilha: `Tabela Map Dent`
        // },
        // {
        //     nomeForn: 'dental sorriso', 
        //     nomePlanilha: 'Dental Sorriso'
        // },
        // {
        //     nomeForn: 'lab. sudré', 
        //     nomePlanilha: 'LabSudre'
        // },
        // {
        //     nomeForn: 'marco aurélio', 
        //     nomePlanilha: 'LabMarcoAurelio'
        // },
        // {
        //     nomeForn: 'jc', 
        //     nomePlanilha: 'Lab. JC'
        // },
        // {
        //     nomeForn: 'joão carlos reis', 
        //     nomePlanilha: 'LAb. João Reis'
        // },
        // {
        //     nomeForn: 'roberta', 
        //     nomePlanilha: 'Lab. Roberta'
        // },
        // {
        //     nomeForn: 'neodent',
        //     nomePlanilha: 'Neodent'
        // },
    ];

    for(let m of arrayMateriais) {
        importa(m.nomePlanilha, m.nomeForn, m.inputFile);
    }

})()


