var CONSTANT_ERROR = require('./constants-error.js')

module.exports.validarCodigoJaExistente = async function(nomeClasse, codigo ) {
    let queryFindByCodigo = new Parse.Query(nomeClasse);
    queryFindByCodigo.equalTo('codigo', codigo);
    let objeto = await queryFindByCodigo.first();

    if(objeto) {
        let objErro = CONSTANT_ERROR.CODIGO_JA_EXISTENTE;
        objErro.message = `${objErro.message}. ${nomeClasse}, ${codigo}`;
        throw Object.assign({}, objErro);
    }
}

module.exports.executeValidacaoCodigoJaExistente = async function (request, className) {
    let objeto = request.object;
    let objetoCodigo = objeto.get('codigo');
    if(!objeto.id && objetoCodigo) {
        await module.exports.validarCodigoJaExistente(className, objetoCodigo);
    }
}